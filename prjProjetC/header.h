#ifndef HEADER_H_INCLUDED
#define HEADER_H_INCLUDED

#define MENU_QUIT 5

#define KEY_CHAR_MAX 10
#define KEY_DIM (KEY_CHAR_MAX+1)
static const char keyInit[KEY_DIM] = "abcd" ;


#define ENCMSG_CHAR_MAX (10*KEY_CHAR_MAX)
#define ENCMSG_DIM (ENCMSG_CHAR_MAX+1)
extern char encMsg[ENCMSG_DIM];

extern int choix;

void displayMenu(void);


#endif // HEADER_H_INCLUDED
