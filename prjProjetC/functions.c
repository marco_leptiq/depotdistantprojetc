#include <stdio.h>
#include <stdlib.h>

#include "header.h"

void displayMenu(void)
{
    printf("\n ----- MENU ------\n");
    printf("\n 1 : Enter key");
    printf("\n 2 : Use default key");
    printf("\n 3 : Write text");
    printf("\n 4 : Read encrypted message");
    printf("\n 5 : Quitter");
    printf("\n\tVotre choix : \n");
}
