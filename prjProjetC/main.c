#include <stdio.h>
#include <stdlib.h>

#include "header.h"

//Encrypted Message
char encMsg[ENCMSG_DIM]={};
//encMsg[ENCMSG_CHAR_MAX]='\0';


int main()
{
    FILE *fpKey=NULL;
    FILE *fpEnc=NULL;
    FILE *fpSrc = NULL;

    // MENU
    unsigned int choix;
    do
    {
        displayMenu();
        scanf("%d", &choix);



        if (choix == 2)
        {

            fpKey = fopen("peroq.def", "r");
            if (fpKey==NULL)
            {
                printf("Erreur Ouverture pour lecture Clef Cryptage \"peroq.def\"\n");
                return EXIT_FAILURE;
            }

            char key[KEY_DIM];
            strncpy(key, keyInit, KEY_CHAR_MAX);
            if ( strlen( keyInit ) >= KEY_DIM )
            {
                key[KEY_DIM] = '\0';
            }
            printf("puts(key); : %d\n", puts(key));
            printf("key : %s\n", key);
        }

        if (choix==3)
        {
            fpEnc = fopen("dest.crt", "w+");
            if (fpEnc == NULL)
            {
                printf("Erreur Ouverture pour Ecriture Message Crypt� \"dest.crt\"\n");
                return EXIT_FAILURE;
            }
            //fwrite(, sizeof(), 1, fpEnc);
            fputs(encMsg, fpEnc);
        }


    }while(choix!=MENU_QUIT);








    fpSrc = fopen("source.txt", "r");
    if (fpSrc==NULL)
    {
        printf("Erreur Ouverture pour lecture Source \"source.txt\"\n");
        return EXIT_FAILURE;
    }

    char cSrc;
    fread(&cSrc, sizeof(char), 1, fpSrc);
    if (feof(fpSrc))
    {
        printf("Fichier vide !!!\n");
    }
    while(!feof(fpSrc))
    {
        fread(&cSrc, sizeof(cSrc), 1, fpSrc);
        if (feof(fpSrc))
        {
            printf("Fin Fichier fpSrc !!!\n");
        }
    }







    //closing fpSrc function
    int ret_fpSrc = fclose(fpSrc);
    if (ret_fpSrc != 0)
    {
        printf("Erreur Closing fpSrc\n");
        return EXIT_FAILURE;
    }

    //closing fpKey function
    int ret_fpKey = fclose(fpKey);
    if (ret_fpKey != 0)
    {
        printf("Erreur Closing fpKey\n");
        return EXIT_FAILURE;
    }

    //closing fpEnc function
    int ret_fpEnc = fclose(fpEnc);
    if (ret_fpEnc != 0)
    {
        printf("Erreur Closing fpEnc\n");
        return EXIT_FAILURE;
    }



    return 0;
}
